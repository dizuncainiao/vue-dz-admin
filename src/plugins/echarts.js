import Vue from 'vue'

import 'echarts/theme/infographic.js'
import 'echarts/theme/blue.js'
// 按需加载
const ECharts = require('echarts/lib/echarts')

require('echarts/lib/chart/line')
require('echarts/lib/chart/bar')
require('echarts/lib/chart/pie')
require('echarts/lib/chart/radar')

require('echarts/lib/component/tooltip')
require('echarts/lib/component/title')
require('echarts/lib/component/legend')

Vue.prototype.$echarts = ECharts
