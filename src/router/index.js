import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: () => import(/* webpackChunkName: "login" */ '../views/Login.vue')
  },
  {
    path: '/home',
    name: 'Home',
    component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue'),
    redirect: { name: 'TableToolsDemo' },
    children: [
      {
        path: 'tableToolsDemo',
        name: 'TableToolsDemo',
        component: () => import(/* webpackChunkName: "ATable" */ '../views/antDemo/TableToolsDemo.vue'),
        meta: { title: '表格自定义工具栏' }
      },
      {
        path: 'arcGisLine',
        name: 'ArcGisLine',
        component: () => import(/* webpackChunkName: "arcgisLine" */ '../views/arcgisDemo/ArcGisLine.vue'),
        meta: { title: 'arcgis画路径' }
      },
      {
        path: 'arcGisPoint',
        name: 'ArcGisPoint',
        component: () => import(/* webpackChunkName: "arcgisPoint" */ '../views/arcgisDemo/ArcGisPoint.vue'),
        meta: { title: 'arcgis画点' }
      },
      {
        path: 'arcGisSwitchMap',
        name: 'ArcGisSwitchMap',
        component: () => import(/* webpackChunkName: "arcgisClickGraphic" */ '../views/arcgisDemo/ArcGisSwitchMap.vue'),
        meta: { title: 'arcgis切换底图' }
      },
      {
        path: 'arcGisComponent',
        name: 'ArcGisComponent',
        component: () => import(/* webpackChunkName: "arcgisComponent" */ '../views/arcgisDemo/ArcgisMapComponent.vue'),
        meta: { title: 'arcgis组件封装' }
      },
      {
        path: 'radarChart',
        name: 'RadarChart',
        component: () => import(/* webpackChunkName: "radarChart" */ '../views/g2plot/RadarChart.vue'),
        meta: { title: 'arcgis组件封装' }
      },
      {
        path: 'radarChart2',
        name: 'RadarChart2',
        component: () => import(/* webpackChunkName: "radarChart2" */ '../views/g2plot/RadarChart2.vue'),
        meta: { title: 'arcgis组件封装' }
      },
      {
        path: 'dzMindMap',
        name: 'DzMindMap',
        component: () => import(/* webpackChunkName: "dzMindMap" */ '../views/mindMap/DzMindMap.vue'),
        meta: { title: 'dz-mind-map示例' }
      }
    ]
  }
]

const router = new VueRouter({
  routes,
  scrollBehavior (to, from, savedPosition) {
    return savedPosition || {
      x: 0,
      y: 0
    }
  }
})

router.beforeEach((to, from, next) => {
  NProgress.start()
  store.commit('setSelectKeys', [to.name])
  next()
})

router.afterEach(() => {
  NProgress.done()
})

export default router
