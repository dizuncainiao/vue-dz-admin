export default [{
  name: '如何学习D3',
  children: [{
    name: '预备知识',
    children: [{
      name: 'HTML & CSS',
      bgColor: '#54B2FD',
      url: 'https://www.baidu.com',
      id: '0-0-0',
      gKey: 3,
      color: '#a6cee3',
      size: [21, 89],
      left: false
    }, {
      name: 'JavaScript',
      id: '0-0-1',
      gKey: 4,
      color: '#a6cee3',
      size: [21, 75],
      left: false,
      bgColor: '#FA8072'
    }, {
      name: 'DOM',
      id: '0-0-2',
      gKey: 5,
      color: '#a6cee3',
      size: [21, 50],
      left: false,
      bgColor: '#91A0F0'
    }, {
      name: 'SVG',
      id: '0-0-3',
      gKey: 6,
      color: '#a6cee3',
      size: [21, 44],
      left: false,
      bgColor: '#54B2FD'
    }, {
      name: 'test',
      id: '0-0-4',
      gKey: 7,
      color: '#a6cee3',
      size: [21, 41],
      left: false,
      bgColor: '#01BDD5'
    }],
    id: '0-0',
    gKey: 2,
    color: '#a6cee3',
    size: [21, 70],
    left: false
  }, {
    name: '安装',
    _children: [],
    id: '0-1',
    gKey: 8,
    color: '#1f78b4',
    size: [21, 46],
    left: false,
    children: [{
      name: '折叠节点',
      id: '0-1-0',
      gKey: 9,
      color: '#1f78b4',
      size: [21, 70],
      left: false,
      bgColor: '#FA8072'
    }],
    bgColor: '#8BC34A'
  }, {
    name: '入门',
    children: [{
      name: '选择集',
      id: '0-2-0',
      gKey: 11,
      color: '#b2df8a',
      size: [21, 58],
      left: true,
      bgColor: '#91A0F0'
    }, {
      name: 'test',
      id: '0-2-1',
      gKey: 12,
      color: '#b2df8a',
      size: [21, 41],
      left: true,
      bgColor: '#91A0F0'
    }, {
      name: '绑定数据',
      id: '0-2-2',
      gKey: 13,
      color: '#b2df8a',
      size: [21, 70],
      left: true,
      bgColor: '#54B2FD'
    }, {
      name: '添加删除元素',
      id: '0-2-3',
      gKey: 14,
      color: '#b2df8a',
      size: [21, 94],
      left: true,
      bgColor: '#01BDD5'
    }, {
      name: '简单图形',
      children: [{
        name: '柱形图',
        id: '0-2-4-0',
        gKey: 16,
        color: '#b2df8a',
        size: [21, 58],
        left: true,
        bgColor: '#FA8072'
      }, {
        name: '折线图',
        id: '0-2-4-1',
        gKey: 17,
        color: '#b2df8a',
        size: [21, 58],
        left: true,
        bgColor: '#FF9800'
      }, {
        name: '散点图',
        id: '0-2-4-2',
        gKey: 18,
        color: '#b2df8a',
        size: [21, 58],
        left: true,
        bgColor: '#FA8072'
      }],
      id: '0-2-4',
      gKey: 15,
      color: '#b2df8a',
      size: [21, 70],
      left: true,
      bgColor: '#FF9800'
    }, {
      name: '比例尺',
      id: '0-2-5',
      gKey: 19,
      color: '#b2df8a',
      size: [21, 58],
      left: true,
      bgColor: '#91A0F0'
    }, {
      name: '生成器',
      id: '0-2-6',
      gKey: 20,
      color: '#b2df8a',
      size: [21, 58],
      left: true,
      bgColor: '#FA8072'
    }, {
      name: '过渡',
      id: '0-2-7',
      gKey: 21,
      color: '#b2df8a',
      size: [21, 46],
      left: true,
      bgColor: '#01BDD5'
    }],
    left: true,
    id: '0-2',
    gKey: 10,
    color: '#b2df8a',
    size: [21, 46],
    bgColor: '#91A0F0'
  }, {
    name: '进阶',
    left: true,
    id: '0-3',
    gKey: 22,
    color: '#33a02c',
    size: [21, 46],
    bgColor: '#FA8072'
  }, {
    name: '一级节点',
    children: [{
      name: '子节点1',
      id: '0-4-0',
      gKey: 24,
      color: '#fb9a99',
      size: [21, 64],
      left: false,
      bgColor: '#FF9800'
    }, {
      name: '子节点2',
      id: '0-4-1',
      gKey: 25,
      color: '#fb9a99',
      size: [21, 64],
      left: false,
      bgColor: '#FF9800'
    }, {
      name: '子节点3',
      id: '0-4-2',
      gKey: 26,
      color: '#fb9a99',
      size: [21, 64],
      left: false,
      bgColor: '#FA8072'
    }],
    id: '0-4',
    gKey: 23,
    color: '#fb9a99',
    size: [21, 70],
    left: false,
    bgColor: '#01BDD5'
  }],
  id: '0',
  gKey: 1,
  size: [21, 107],
  left: false,
  bgColor: '#FF9800'
}]
