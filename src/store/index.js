import Vue from 'vue'
import Vuex from 'vuex'
// vuex 持久化插件
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // 导航菜单当前选中的key
    selectKeys: [],
    // 去除导航和底部的页面高度
    layoutHeight: 0,
    globalSelectVal: null
  },
  mutations: {
    setSelectData (state, val) {
      state.globalSelectVal = val
    },
    setSelectKeys (state, keys) {
      state.selectKeys = keys
    },
    setLayoutHeight (state, height) {
      state.layoutHeight = height
    }
  },
  actions: {},
  modules: {},
  plugins: [createPersistedState({ storage: window.sessionStorage })]
})
