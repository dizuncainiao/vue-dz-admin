const webpack = require('webpack')

module.exports = {
  publicPath: process.env.NODE_ENV === 'development' ? '' : '/vue-dz-admin',
  outputDir: 'www',
  productionSourceMap: false,
  devServer: {
    open: true,
    port: 8888
  },
  css: {
    loaderOptions: {
      less: {
        modifyVars: {
          'primary-color': '#0088f0',
          'link-color': '#0088f0',
          'border-radius-base': '3px'
        },
        javascriptEnabled: true
      }
    }
  },
  configureWebpack: {
    plugins: [
      new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/)
    ]
  }
}
